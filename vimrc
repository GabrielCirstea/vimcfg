" default za to fold or unfold items
" Basic settings -----------{{{
syntax on

set noerrorbells
set tabstop=4 softtabstop=4
set shiftwidth=4
set number
set relativenumber
set nowrap
set smartcase
set smartindent
" set noswapfile				" o sa las putin swap file
set nobackup
set hidden
" NO string path
" set directory=~/.vim/swapdir//
set undodir=~/.local/state/vim/undodir
set viminfo+=n~/.vim/viminfo
set undofile
set incsearch

" bara la 80 de caractere
set colorcolumn=80
highlight ColorColumn ctermbg=0 guibg=lightgrey

" for some build in plugins
filetype plugin indent on

set backspace=indent,eol,start

" deep search in the current folder and subfolders with :find
set path=.,**

let $RTP=split(&runtimepath, ',')[0]
" }}}

" vim file browser {{{
let g:netrw_liststyle=3
let g:netrw_altv=1
" }}}

call plug#begin('~/.vim/plugged')

Plug 'morhetz/gruvbox'
Plug 'rust-lang/rust.vim'
Plug 'tomlion/vim-solidity'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-fugitive'

call plug#end()

colorscheme gruvbox
set background=dark
highlight Normal guibg=NONE ctermbg=NONE

" Mapping -------- {{{
let mapleader = "\\"
" deschide vimrc intr-un split separat
nnoremap <leader>ev :vsplit $MYVIMRC<CR>
" da source la vimrc
nnoremap <leader>sv :source $MYVIMRC<CR>
" deschide buffer-un anterior in a vsplit la stanga
nnoremap <leader>vl :leftabove vsplit #<CR>
" deschide buffer-un anterior in a vsplit la dreapta
nnoremap <leader>vr :rightbelow vsplit #<CR>
" deschide buffer-un anterior in a split sus
nnoremap <leader>sa :leftabove split #<CR>
" deschide buffer-un anterior in a split jos
nnoremap <leader>sb :rightbelow split #<CR>
" marcheaza spatiile de la finalul liniei
nnoremap <leader>w :match Error /  \+$/<CR>
nnoremap <leader>W :match none<CR>
" cauta magic
nnoremap <leader>/ /\v
" opreste highlight-ul pentru search
nnoremap <leader>h :nohlsearch<CR>
" muta linia mai jos
nnoremap - ddp
" muta linia mai sus
nnoremap _ ddkP
" face cuvantul sa fie scris cu litere MARI in insert mode
inoremap <c-u> <esc>viwUea
" acelas lucru dar in normal mode
nnoremap <c-u> viwUe

" pune cuvantul pe care se afla cursorul intre ghilimile
nnoremap <leader>" viw<esc>a"<esc>bi"<esc>lel
nnoremap <leader>' viw<esc>a'<esc>bi'<esc>lel

" pune selectia intre ghilimele
vnoremap <leader>" <esc>`<i"<esc>`>a"<esc>
vnoremap <leader>' <esc>`<i'<esc>`>a'<esc>

" noul <esc>
inoremap jk <esc>
" Poate o sa dezactivam si vechiu <esc>
inoremap <esc> <nop>

" maparea operatorilor de miscare
" actioneaza pe toata paranteza urmatoare respectiv anterioara
 onoremap an( :<c-u>normal! f(va(<CR>
 onoremap al( :<c-u>normal! F)va(<CR>
"
 onoremap in( :<c-u>normal! f(vi(<CR>
 onoremap il( :<c-u>normal! F)vi(<CR>

" display strlen(selection)
vnoremap <leader>l "ly <esc>:echo "len("@l")=" strlen(@l)<CR>

" FZF map
if exists(':Files')
	nnoremap <leader>F :Files<CR>
else
	nnoremap <leader>F :FZF<CR>
endif

if exists(':Buffers')
	cnoreabbrev ls Buffers
endif
 " }}}

" abreviatii - foarte bune pentru corectare
iabbrev @@ gabrielcirstea@protonmail.com
" pentru C++
iabbrev ednl endl

" LaTeX -----{{{
augroup filetype_latex
	autocmd!
	let maplocalleader = ";"
	autocmd FileType tex nnoremap <buffer> <F5> :! pdflatex %<CR>
	autocmd FileType tex nnoremap <buffer> <F6> :! zathura %:r.pdf &<CR>
	autocmd FileType tex inoremap <buffer> <localleader>m \(\)<ESC>hi
	autocmd FileType tex inoremap <buffer> <localleader>* \cdot
	autocmd FileType tex inoremap <buffer> <localleader>u \underline{}<ESC>i
	autocmd FileType tex inoremap <buffer> <localleader>M \[\]<ESC>hi
	autocmd FileType tex inoremap <buffer> <localleader>u \underline{}<ESC>i
	autocmd FileType tex inoremap <buffer> <localleader>s \mathbb{}<ESC>i
augroup END
" }}}

" Marddwon file settings ------------------------ {{{
augroup markdown_file
	autocmd!
	" edit inside/aroud heading
	autocmd FileType markdown onoremap <buffer> ih :<c-u>execute "normal! ?^[-=]\\{2,}$\r:nohlsearch\rkvg_"<CR>
	autocmd FileType markdown onoremap <buffer> ah :<c-u>execute "normal! ?^[-=]\\{2,}$\r:nohlsearch\rg_vk0"<CR>
	autocmd FileType markdown setlocal statusline+=\ Incearca\ sa\ heading
	autocmd FileType markdown nnoremap <buffer> <F5> :! ~/Scripts/mark-randr.sh %<CR>
augroup END
" }}}

" VimScrip file settings -------------{{{
augroup filetype_vim
	autocmd!
	autocmd FileType vim setlocal foldmethod=marker
augroup END
" }}}

" StatusLine ----------- {{{
" status line - bara de jos
set statusline=Gabi\ editeaza:%f	" numele fisierului
set statusline+=%=					" ne ducem in partea dreapta
set statusline+=Line:%l/%L			" linia curenta / liniile totale
set statusline+=,\ Col:%c			" coloana, cu 2 sptii minim
set statusline+=%4p%%				" procentul in pagina a liniilor
" }}}

" R lang -----{{{
" requires da fifo file to work
" mkfifo pipe # pipe is the name of the fifo, can be whatever
" tail -f pipe | R --no-save
augroup R_lang
	autocmd!
	let maplocalleader = ";"
	autocmd FileType r nnoremap <buffer> <F5> :redir >> pipe<CR>:echo "source(\"".@%."\")"<CR>:redir END<CR>
	autocmd FileType r nnoremap <buffer> <localleader><F5> :redir >> pipe<CR>"syy:echo @s<CR>:redir END<CR>
	autocmd FileType r nnoremap <buffer> <localleader>w :redir >> pipe<CR>:echo expand("<cword>")<CR>:redir END<CR>
	autocmd FileType r nnoremap <buffer> <localleader>W :redir >> pipe<CR>:echo expand("<cWORD>")<CR>:redir END<CR>

augroup END
" }}}

" functiile mele

" insereaza tagurile html de intceput
function! HtmlTemplate()
	read ~/Templates/template.html
endfunction

function! CppTemplate()
	read ~/Templates/main.cpp
endfunction

function! PythonTemplate()
	read ~/Templates/python.py
endfunction
