# My vimrc file

I finally made a vimrc for myself, and i'm still working on it.

##  Is something sepcial?

(Fo me, it is)

Long story short:

* Some basic settings to adjust the text on the screen
* some mapping to manipulate text:
	* to move lines up or down: -, _
	* to put text between quotation marks: \", \'
	* including some operators mapping
	* turnoff search highlight: <leader>h
	* quick magic search <leader>/
	* match whitespaces: <leader>w
	* unmatch whitespaces: <leader>W
* Mapping for html, python and markdown files
* ....and a little status line, visible only while split
* Quick split with for:
	* split above or belor: 
	```
	<leader>sa(/b)
	```
	* vertical split left or right: 
	```
	<leader>vl(/r)
	```

### tmux

In .bashrc:

```
alias tmux="TERM=screen-256color-bce tmux"
```

## Instalation

install.sh will make simbolic links to .vimrc and .vim dir, will also check if
undodir and swapdir are created.

The script will copy the existent(if there is) .vimrc and .vim in .vimrc1 and .vim1
