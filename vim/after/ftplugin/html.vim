setlocal nowrap

let maplocalleader = ";"
nnoremap <buffer> <F5> :! $BROWSER %<CR>
inoremap <buffer> <localleader>i <em></em> <++><esc>2F>a
inoremap <buffer> <localleader>b <strong></strong> <++><ESC>2F>a
inoremap <buffer> <localleader>p <p></p> <CR><++><ESC>?<p><CR>f>a
" scri un cuvant si face div din clasa respectiva
inoremap <buffer> <localleader>c <ESC>viw"-y"_diwa<div class="<ESC>"-pa"><CR></div><CR><++><ESC>?</div><CR>O
" scri un cuvant si il face tag: scri section si face <section></section>
inoremap <buffer> <localleader>s <ESC>viw"-y"_diwa<<ESC>"-pa>s</<ESC>"-pa><CR><++><ESC>?>s<<CR>l"_s
inoremap <buffer> <localleader><space> <ESC>/<++><CR>"_c4l
