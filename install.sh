#!/usr/bin/sh
# make the links to the .vimrc and the .vim folder
# also check the undodir and swapdir directories

[ -d ~/.local/state/vim/undodir ] || mkdir -p ~/.local/state/vim/undodir
[ -d vim/swapdir ] || mkdir vim/swapdir

[ -f ~/.vimrc ] && mv ~/.vimrc ~/.vimrc1
[ -d ~/.vim ] && mv ~/.vim ~/.vim1

[ -L ~/.vim ] && rm ~/.vim
[ -L ~/.vimrc ] && rm ~/.vimrc

ln -s $(pwd)/vim ~/.vim
ln -s $(pwd)/vimrc ~/.vim/vimrc
